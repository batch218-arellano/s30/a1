// Activity

//1 

db.fruits.aggregate([
		 {$match: {onSale: true}},
		 {$count: "fruits_on_sale"}
		]);

//2 
db.fruits.aggregate([
	   {$match: {stock:{$gte:20}}},
	   {$count: "fruits_more_twenty"}
		]);


//3 
db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id:"$supplier_id", fruits_on_sale_average: {$avg: "$price"}}},
   ]);


//4

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id:"$supplier_id", fruits_on_sale_price: {$max: "$price"}}},
   ]);



//5 

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id:"$supplier_id", fruits_on_sale_price: {$min: "$price"}}},
   ]);

